package jv21701.bondarenko.mysocnet.web.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jv21701.bondarenko.mysocnet.web.objects.UploadedFile;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ContentValidation implements Validator {

    private final int MAX_MEDIANAME = 30;

    private final long MAX_FILE_SIZE = 5242880 * 2;

    private static final String IMAGE_PATTERN
            = ".*?\\.(jpg|png)";

    private static final String VIDEO_PATTERN
            = ".*?\\.mp4";

    private static final String AUDIO_PATTERN
            = ".*?\\.mp3";

    private static final String CORRECTFILE_PATTERN
            = ".*?\\.(jpg|png|mp4|mp3)";

    public boolean isPicture(String testString) {
        Pattern p = Pattern.compile(IMAGE_PATTERN);
        Matcher m = p.matcher(testString);
        return m.matches();
    }

    public boolean isVideo(String testString) {
        Pattern p = Pattern.compile(VIDEO_PATTERN);
        Matcher m = p.matcher(testString);
        return m.matches();
    }

    public boolean isAudio(String testString) {
        Pattern p = Pattern.compile(AUDIO_PATTERN);
        Matcher m = p.matcher(testString);
        return m.matches();
    }

    public boolean isCorrectFile(String testString) {
        Pattern p = Pattern.compile(CORRECTFILE_PATTERN);
        Matcher m = p.matcher(testString);
        return m.matches();
    }

    @Override
    public boolean supports(Class<?> type) {
        return UploadedFile.class.equals(type);
    }

    @Override
    public void validate(Object uploadedFile, Errors errors) {

        UploadedFile file = (UploadedFile) uploadedFile;

        if (file.getFile().getSize() == 0) {
            errors.rejectValue("file", "file.valid.file");
        } else if (file.getFile().getSize() > MAX_FILE_SIZE) {
            errors.rejectValue("file", "file.valid.size");
        } else if (!isCorrectFile(file.getFile().getOriginalFilename())) {
            errors.rejectValue("file", "file.valid.format");
        } else if (file.getFile().getOriginalFilename().length() > MAX_MEDIANAME) {
            errors.rejectValue("file", "file.valid.namelenth");
        }
    }

}
