package jv21701.bondarenko.mysocnet.web.controllers;

import java.util.ArrayList;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.UsersDao;
import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.mail.bean.Mail;
import jv21701.bondarenko.mysocnet.mail.service.MailService;
import jv21701.bondarenko.mysocnet.service.AdminUsersObject;
import jv21701.bondarenko.mysocnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class UsertableController {

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;
    
    @Autowired
    private UsersDao usersDao;

    @Transactional
    @RequestMapping(value = "usertable", method = RequestMethod.GET)
    public String foundUser(ModelMap modelMap) {
        List<AdminUsersObject> users = new ArrayList<>();
        for (Users user : usersDao.getUsers(0, usersDao.getUsersCount())) {
            users.add(new AdminUsersObject(user));
        }
        modelMap.addAttribute("users", users);
        return "usertable";
    }

    @Transactional
    @RequestMapping(value = "banUser", method = RequestMethod.POST)
    public String banUser(@RequestParam(value = "banUser") int banUser) {
        Users bannedUser = userService.byId(banUser);
        bannedUser.setRegstatus(false);
        userService.update(bannedUser);
        Mail mail = new Mail();
        mail.setMailFrom("noreply@domain.com");
        mail.setMailTo(bannedUser.getLogin());
        mail.setMailSubject("Ban");
        mail.setMailContent("Your account was banned");
        mailService.sendEmail(mail);
        return "redirect:usertable";
    }

    @Transactional
    @RequestMapping(value = "unBanUser", method = RequestMethod.POST)
    public String unBanUser(@RequestParam(value = "unBanUser") int unBanUser) {
        Users unBannedUser = userService.byId(unBanUser);
        unBannedUser.setRegstatus(true);
        userService.update(unBannedUser);
        Mail mail = new Mail();
        mail.setMailFrom("noreply@domain.com");
        mail.setMailTo(unBannedUser.getLogin());
        mail.setMailSubject("Unban");
        mail.setMailContent("Your account was unbanned");
        mailService.sendEmail(mail);
        return "redirect:usertable";
    }
}
