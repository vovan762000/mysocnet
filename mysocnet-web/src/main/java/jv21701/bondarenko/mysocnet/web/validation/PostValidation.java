package jv21701.bondarenko.mysocnet.web.validation;

import jv21701.bondarenko.mysocnet.entityes.Posts;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PostValidation implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        return Posts.class.equals(type);
    }

    @Override
    public void validate(Object post, Errors errors) {
        Posts newPost = (Posts) post;
        if (newPost.getPost().length() > 500) {
            errors.rejectValue("post", "post.valid.lenth");
        }
         if (newPost.getPost() == null || newPost.getPost().trim() == "") {
            errors.rejectValue("post", "post.valid.post");
        }
    }

}
