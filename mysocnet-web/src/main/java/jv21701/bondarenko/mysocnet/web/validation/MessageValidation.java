
package jv21701.bondarenko.mysocnet.web.validation;

import jv21701.bondarenko.mysocnet.entityes.Messages;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class MessageValidation implements Validator{

    @Override
    public boolean supports(Class<?> type) {
        return Messages.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
               Messages message = (Messages) o;
        if (message.getInMessage().length() > 250) {
            errors.rejectValue("inMessage", "messages.valid.lenth");
        }
         if (message.getInMessage() == null || message.getInMessage().trim() == "") {
            errors.rejectValue("inMessage", "messages.valid.post");
        }
    }
    
}
