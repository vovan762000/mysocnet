package jv21701.bondarenko.mysocnet.web.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jv21701.bondarenko.mysocnet.dao.UsersDao;
import jv21701.bondarenko.mysocnet.entityes.Comments;
import jv21701.bondarenko.mysocnet.entityes.CommentsMedia;
import jv21701.bondarenko.mysocnet.entityes.Media;
import jv21701.bondarenko.mysocnet.entityes.Posts;
import jv21701.bondarenko.mysocnet.entityes.UserLikeMedia;
import jv21701.bondarenko.mysocnet.entityes.UserLikePost;
import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.service.AdminUsersObject;
import jv21701.bondarenko.mysocnet.service.CommentsMediaService;
import jv21701.bondarenko.mysocnet.service.CommentsService;
import jv21701.bondarenko.mysocnet.service.FriendsService;
import jv21701.bondarenko.mysocnet.service.MediaService;
import jv21701.bondarenko.mysocnet.service.PostsService;
import jv21701.bondarenko.mysocnet.service.UserLikeMediaService;
import jv21701.bondarenko.mysocnet.service.UserLikePostService;
import jv21701.bondarenko.mysocnet.web.validation.CommentsMediaValidation;
import jv21701.bondarenko.mysocnet.web.validation.CommentsValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes({"user", "friends"})
public class HomeController {

    @Autowired
    private UsersDao usersDao;

    @Autowired
    private MediaService mediaService;

    @Autowired
    private PostsService postsService;

    @Autowired
    private CommentsService commentsService;

    @Autowired
    private CommentsMediaService commentsMediaService;

    @Autowired
    private CommentsValidation commentsValidation;

    @Autowired
    private CommentsMediaValidation commentsMediaValidation;

    @Autowired
    private FriendsService friendsService;

    @Autowired
    private UserLikePostService userLikePostService;
    
    @Autowired
    private UserLikeMediaService userLikeMediaService;

    private boolean userDidLike = false;

    @RequestMapping(value = "/login")
    public String getLoginPage(Model model, @RequestParam(value = "error", required = false) String error) {
        if (error != null) {
            model.addAttribute("error", "Your username or password is invalid.");
        }
        return "login";
    }

    @Transactional
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(ModelMap modelMap) {
        modelMap.addAllAttributes(getContentMap());
        modelMap.addAttribute("user", getPrincipal());
        modelMap.put("comment", new Comments());
        modelMap.put("commentMedia", new CommentsMedia());
        modelMap.put("friends", friendsService.list());
        return "home";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }

    @Transactional
    @RequestMapping(value = "addLikePost", method = RequestMethod.POST)
    public String addLikePost(@RequestParam("postid") Integer postid,
            @SessionAttribute("user") Users user) {

        userDidLike = false;
        for (UserLikePost userLikePost : userLikePostService.list()) {
            if (userLikePost.getPostid() == postid
                    && userLikePost.getUserid() == user.getId()) {
                userDidLike = true;
                break;
            }
        }

        if (!postsService.byId(postid).getUsersId().equals(user)
                && !userDidLike) {
            Posts posts = postsService.byId(postid);
            posts.setLikeCount(postsService.byId(postid).getLikeCount() + 1);
            postsService.update(posts);
            UserLikePost userLikePost = new UserLikePost();
            userLikePost.setPostid(postid);
            userLikePost.setUserid(user.getId());
            userLikePostService.save(userLikePost);
        }
        return "redirect:home";
    }

    @Transactional
    @RequestMapping(value = "addLikeMedia", method = RequestMethod.POST)
    public String addLikeMedia(@RequestParam("mediaid") Integer mediaid,
            @SessionAttribute("user") Users user) {
        
        userDidLike = false;
        
         for (UserLikeMedia userLikeMedia : userLikeMediaService.list()) {
            if (userLikeMedia.getMediaId() == mediaid
                    && userLikeMedia.getUserId() == user.getId()) {
                userDidLike = true;
                break;
            }
        }
         
        if (!mediaService.byId(mediaid).getUsersId().equals(user)
                && !userDidLike) {
            Media media = mediaService.byId(mediaid);
            media.setLikeCount(mediaService.byId(mediaid).getLikeCount() + 1);
            mediaService.update(media);
            UserLikeMedia userLikeMedia = new UserLikeMedia();
            userLikeMedia.setMediaId(mediaid);
            userLikeMedia.setUserId(user.getId());
            userLikeMediaService.save(userLikeMedia);
        }
        return "redirect:home";
    }

    @Transactional
    @RequestMapping(value = "addComment", method = RequestMethod.POST)
    public String addComment(@RequestParam("postid") Integer postid,
            ModelMap modelMap,
            @SessionAttribute("user") Users user,
            @ModelAttribute(value = "comment") Comments comment,
            BindingResult result) {
        commentsValidation.validate(comment, result);
        if (result.hasErrors()) {
            modelMap.addAllAttributes(getContentMap());
            return "home";
        }
        comment.setPostsId(postsService.byId(postid));
        comment.setDateCreated(new Date());
        comment.setUserComName(user.getFirstname() + " " + user.getLastname());
        commentsService.save(comment);
        return "redirect:home";
    }

    @Transactional
    @RequestMapping(value = "addCommentsMedia", method = RequestMethod.POST)
    public String addCommentsMedia(@RequestParam("postid") Integer postid,
            ModelMap modelMap,
            @SessionAttribute("user") Users user,
            @ModelAttribute(value = "commentMedia") CommentsMedia commentMedia,
            BindingResult result) {
        commentsMediaValidation.validate(commentMedia, result);
        if (result.hasErrors()) {
            modelMap.addAllAttributes(getContentMap());
            return "home";
        }
        commentMedia.setMedia(mediaService.byId(postid));
        commentMedia.setUserComName(user.getFirstname() + " " + user.getLastname());
        commentMedia.setDateCreated(new Date());
        commentsMediaService.save(commentMedia);
        return "redirect:home";
    }

    private Map getContentMap() {
        Map<String, Object> contentMap = new HashMap<>();
        Map<Date, Object> mapContent = new TreeMap<>();
        List<Object> content = new ArrayList<>();

        for (Media media : mediaService.list()) {
            mapContent.put(media.getDateCreated(), media);
        }
        for (Posts post : postsService.list()) {
            mapContent.put(post.getDateCreated(), post);
        }
        for (Map.Entry<Date, Object> entry : mapContent.entrySet()) {
            content.add(0, entry.getValue());
        }

        List<AdminUsersObject> users = new ArrayList<>();
        for (Users user : usersDao.getUsers(0, usersDao.getUsersCount())) {
            users.add(new AdminUsersObject(user));
        }

        List<Comments> comments = new ArrayList<>();
        for (Comments comment : commentsService.list()) {
            comments.add(0, comment);
        }

        List<CommentsMedia> commentsMedia = new ArrayList<>();
        for (CommentsMedia commMedia : commentsMediaService.list()) {
            commentsMedia.add(0, commMedia);
        }
        contentMap.put("users", users);
        contentMap.put("content", content);
        contentMap.put("comments", comments);
        contentMap.put("commentsMedia", commentsMedia);
        return contentMap;
    }

    private UserDetails getPrincipal() {
        UserDetails userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = (UserDetails) principal;
        }
        return userName;
    }

}
