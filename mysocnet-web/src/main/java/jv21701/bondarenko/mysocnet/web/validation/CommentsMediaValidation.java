
package jv21701.bondarenko.mysocnet.web.validation;

import jv21701.bondarenko.mysocnet.entityes.CommentsMedia;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class CommentsMediaValidation implements Validator{

    @Override
    public boolean supports(Class<?> type) {
        return CommentsMedia.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
                      CommentsMedia commentsMedia = (CommentsMedia) o;
        if (commentsMedia.getCommentsMedia().length() > 50) {
            errors.rejectValue("commentsMedia", "comment.valid.lenth");
        }
         if (commentsMedia.getCommentsMedia() == null || commentsMedia.getCommentsMedia().trim() == "") {
            errors.rejectValue("commentsMedia", "comment.valid.post");
        }
    }
    
}
