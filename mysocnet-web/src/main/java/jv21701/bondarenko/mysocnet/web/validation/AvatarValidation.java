package jv21701.bondarenko.mysocnet.web.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jv21701.bondarenko.mysocnet.web.objects.UploadedFile;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class AvatarValidation implements Validator {

    private static final String IMAGE_PATTERN
            = ".*?\\.(jpg|png)";

    @Override
    public void validate(Object uploadedFile, Errors errors) {

        UploadedFile file = (UploadedFile) uploadedFile;

        if (file.getFile().getSize() == 0) {
            errors.rejectValue("file", "file.valid.file");
        } else if (file.getFile().getSize() > 5242880) {
            errors.rejectValue("file", "file.valid.size");
        } else if (!isPicture(file.getFile().getOriginalFilename())) {
            errors.rejectValue("file", "image.valid.format");
        }

    }

    @Override
    public boolean supports(Class<?> type) {
        return UploadedFile.class.equals(type);
    }

    public boolean isPicture(String testString) {
        Pattern p = Pattern.compile(IMAGE_PATTERN);
        Matcher m = p.matcher(testString);
        return m.matches();
    }
}
