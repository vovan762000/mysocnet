package jv21701.bondarenko.mysocnet.web.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.service.UserService;
import jv21701.bondarenko.mysocnet.web.objects.UploadedFile;
import jv21701.bondarenko.mysocnet.web.validation.AvatarValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class EditController {

    @Autowired
    private UserService userService;

    @Autowired
    private AvatarValidation avatarValidation;

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String getViewEdit(@SessionAttribute("user") Users user) {
        return "edit";

    }

    @Transactional
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String editUser(@SessionAttribute("user") Users user,
            @RequestParam("firstname") String firstname,
            @RequestParam("lastname") String lastname) {

        Users newUser = userService.byId(user.getId());
        newUser.setFirstname(firstname);
        newUser.setLastname(lastname);
        userService.update(newUser);
//        Authentication authentication = new UsernamePasswordAuthenticationToken(newUser,
//                newUser.getFirstname(), newUser.getAuthorities());
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//        List<AdminUsersObject> newUsers = new ArrayList<>();
//        for (Users oldUser : usersDao.getUsers(0, usersDao.getUsersCount())) {
//            newUsers.add(new AdminUsersObject(oldUser));
//        }

        return "redirect:logout";
    }

    @Transactional
    @RequestMapping(value = "uploadAvatar", method = RequestMethod.POST)
    public String uploadAvatar(@SessionAttribute("user") Users user,
            @ModelAttribute("uploadAvatar") UploadedFile uploadAvatar,
            BindingResult result) {

        String fileName = null;

        MultipartFile file = uploadAvatar.getFile();

        avatarValidation.validate(uploadAvatar, result);

        if (result.hasErrors()) {
            return "edit";
        } else {
            try {
                byte[] bytes = file.getBytes();

                fileName = file.getOriginalFilename();

                String rootPath = System.getProperty("catalina.home");
                File dir = new File(rootPath + File.separator + "tmpFiles");

                if (!dir.exists()) {
                    dir.mkdirs();
                }

                File loadFile = new File(dir.getAbsolutePath() + File.separator + fileName);

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(loadFile));
                stream.write(bytes);
                stream.flush();
                stream.close();

                javaxt.io.Image image = new javaxt.io.Image(dir.getAbsolutePath() + File.separator + fileName);
                image.resize(120, 120);
                byte[] newBytes = image.getByteArray();
                BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(loadFile));
                stream1.write(newBytes);
                stream1.flush();
                stream1.close();

                Users userNewAva = userService.byId(user.getId());
                userNewAva.setAvatar("files" + File.separator + file.getOriginalFilename());
                userService.update(userNewAva);
//                Authentication authentication = new PreAuthenticatedAuthenticationToken(userNewAva,
//                       userNewAva.getAvatar(),userNewAva.getAuthorities());
//                SecurityContextHolder.getContext().setAuthentication(authentication);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return "redirect:logout";
        }
    }
}
