
package jv21701.bondarenko.mysocnet.web.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import jv21701.bondarenko.mysocnet.entityes.Messages;
import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.service.MessagesService;
import jv21701.bondarenko.mysocnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class CorrespController {
    @Autowired
    private UserService userService;

    @Autowired
    private MessagesService messagesService;

    private Map<Date, Messages> userMesMap = new TreeMap<>();
    private List<Messages> messList = new ArrayList<>();

    @Transactional
    @RequestMapping(value = "correspondence", method = RequestMethod.GET)
    public String foundUser(@SessionAttribute("user") Users user, @RequestParam(value = "id") Integer id,
            ModelMap modelMap) {

        messList.clear();
        userMesMap.clear();

        for (Messages messages : messagesService.list()) {
            if (messages.getUsersId().getId() == user.getId()
                    && messages.getToUserId() == id) {
                userMesMap.put(messages.getDateCreated(), messages);
            }
            if (messages.getUsersId().getId() == id
                    && messages.getToUserId() == user.getId()) {
                userMesMap.put(messages.getDateCreated(), messages);
            }
        }
        for (Map.Entry<Date, Messages> entry : userMesMap.entrySet()) {
            messList.add(0, entry.getValue());
        }
        modelMap.addAttribute("messList", messList);
        modelMap.addAttribute("foundUser", userService.byId(id));
        modelMap.addAttribute("message", new Messages());
        return "correspondence";
    }
}
