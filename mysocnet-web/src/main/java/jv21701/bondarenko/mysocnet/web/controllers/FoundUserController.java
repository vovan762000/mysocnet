package jv21701.bondarenko.mysocnet.web.controllers;

import java.util.HashMap;
import java.util.Map;
import jv21701.bondarenko.mysocnet.entityes.Friends;
import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.service.FriendsService;
import jv21701.bondarenko.mysocnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class FoundUserController {

    @Autowired
    private UserService userService;

    @Autowired
    private FriendsService friendsService;

    private boolean areFriends = false;
    private Map<Users, Boolean> foundUsers = new HashMap<>();

    @RequestMapping(value = "founduser", method = RequestMethod.GET)
    public String foundUser(ModelMap modelMap) {
        for (Map.Entry<Users, Boolean> foundUser : foundUsers.entrySet()) {
            for (Friends friend : friendsService.list()) {
                if (userService.byId(friend.getFriendid()).equals(foundUser.getKey())) {
                    foundUser.setValue(true);
                }
            }

        }

        modelMap.addAttribute("foundUsers", foundUsers);
        return "founduser";

    }

    @Transactional
    @RequestMapping(value = "founduser", method = RequestMethod.POST)
    public String foundUser(@RequestParam(value = "foundUser") String foundUser,
            @SessionAttribute("user") Users user, ModelMap modelMap) {
        foundUsers.clear();
        String[] arrayName = foundUser.split(" ");
        for (Users wantedUser : userService.list()) {
            if (arrayName[0].equalsIgnoreCase(wantedUser.getFirstname())
                    && arrayName[1].equalsIgnoreCase(wantedUser.getLastname())
                    && !wantedUser.equals(user)) {
                for (Friends friend : friendsService.list()) {
                    if ((friend.getFriendid() == wantedUser.getId()
                            && friend.getUsersId().getId() == user.getId())
                            || (friend.getUsersId().getId() == wantedUser.getId()
                            && friend.getFriendid() == user.getId())) {
                        areFriends = true;
                    }
                }
                foundUsers.put(wantedUser, areFriends);
                areFriends = false;
            }
        }
        if (!foundUsers.isEmpty()) {
            modelMap.addAttribute("foundUsers", foundUsers);
            return "founduser";
        }
        return "redirect:profile";
    }
}
