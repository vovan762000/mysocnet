package jv21701.bondarenko.mysocnet.web.controllers;

import javax.servlet.http.HttpServletRequest;
import jv21701.bondarenko.mysocnet.dto.UserDto;
import jv21701.bondarenko.mysocnet.mail.bean.Mail;
import jv21701.bondarenko.mysocnet.mail.service.MailService;
import jv21701.bondarenko.mysocnet.service.UserService;
import jv21701.bondarenko.mysocnet.web.validation.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private MailService mailService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(userValidator);
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegistrationPage(Model model) {
        model.addAttribute("userForm", new UserDto());
        return "registration";
    }

    @RequestMapping(value = "/regsuccess", method = RequestMethod.GET)
    public String regsuccess() {
        return "regsuccess";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registerUserAccount(@ModelAttribute("userForm") @Validated UserDto userForm,
            BindingResult bindingResult, Model model, HttpServletRequest request) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        } else {

            userService.registerNewUserAccount(userForm);
            String appUrl = request.getScheme() + "://" + request.getServerName();

            Mail mail = new Mail();
            mail.setMailFrom("noreply@domain.com");
            mail.setMailTo(userForm.getLogin());
            mail.setMailSubject("Registration Confirmation");
            mail.setMailContent("To confirm your e-mail address, please click the link below:\n"
                    + appUrl + "/confirm?token=" + userForm.getREGCODE());
            mailService.sendEmail(mail);

            return "redirect:/regsuccess";
        }
    }
}
