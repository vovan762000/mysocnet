package jv21701.bondarenko.mysocnet.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DescriptionController {

    @RequestMapping(value = "description", method = RequestMethod.GET)
    public String getDescription() {
        return "description";
    }
}
