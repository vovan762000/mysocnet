package jv21701.bondarenko.mysocnet.web.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import jv21701.bondarenko.mysocnet.entityes.Messages;
import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.service.MessagesService;
import jv21701.bondarenko.mysocnet.service.UserService;
import jv21701.bondarenko.mysocnet.web.validation.MessageValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class MessageController {

    @Autowired
    private MessagesService messagesService;

    @Autowired
    private UserService usersService;

    @Autowired
    private MessageValidation messageValidation;

    @Transactional
    @RequestMapping(value = "sendMessage", method = RequestMethod.POST)
    public String foundUser(@ModelAttribute(value = "message") Messages message, BindingResult result,
            @SessionAttribute("user") Users user, Model model,
            @RequestParam(value = "foundUser") int foundUserId) {

        model.addAttribute("foundUser", usersService.byId(foundUserId));
        messageValidation.validate(message, result);

        if (result.hasErrors()) {
            model.addAttribute("messList", getMessList(user, foundUserId));
            return "founduser";
        }

        message.setDateCreated(new Date());
        message.setToUserId(foundUserId);
        message.setUsersId(usersService.byId(user.getId()));
        messagesService.save(message);
        model.addAttribute("messList", getMessList(user, foundUserId));
        return "correspondence";
    }

    private List getMessList(Users user, int foundUserId) {
        Map<Date, Messages> userMesMap = new TreeMap<>();
        List<Messages> messList = new ArrayList<>();
        messList.clear();
        userMesMap.clear();
        for (Messages messages : messagesService.list()) {
            if (messages.getUsersId().getId() == user.getId()
                    && messages.getToUserId() == foundUserId) {
                userMesMap.put(messages.getDateCreated(), messages);
            }
            if (messages.getUsersId().getId() == foundUserId
                    && messages.getToUserId() == user.getId()) {
                userMesMap.put(messages.getDateCreated(), messages);
            }
        }
        for (Map.Entry<Date, Messages> entry : userMesMap.entrySet()) {
            messList.add(0, entry.getValue());
        }
        return messList;
    }
}
