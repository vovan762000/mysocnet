package jv21701.bondarenko.mysocnet.web.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import jv21701.bondarenko.mysocnet.entityes.Comments;
import jv21701.bondarenko.mysocnet.entityes.CommentsMedia;
import jv21701.bondarenko.mysocnet.entityes.Media;
import jv21701.bondarenko.mysocnet.entityes.Posts;
import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.service.CommentsMediaService;
import jv21701.bondarenko.mysocnet.service.CommentsService;
import jv21701.bondarenko.mysocnet.service.MediaService;
import jv21701.bondarenko.mysocnet.service.PostsService;
import jv21701.bondarenko.mysocnet.service.UserService;
import jv21701.bondarenko.mysocnet.web.objects.UploadedFile;
import jv21701.bondarenko.mysocnet.web.validation.ContentValidation;
import jv21701.bondarenko.mysocnet.web.validation.PostValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProfileController {
    
    @Autowired
    private PostsService postsService;
    
    @Autowired
    private MediaService mediaService;
    
    @Autowired
    private ContentValidation contentValidation;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private PostValidation postValidation;
    
    @Autowired
    private CommentsMediaService commentsMediaService;
    
    @Autowired
    private CommentsService commentsService;
    
    @RequestMapping(value = "profile")
    @Transactional
    public ModelAndView getProfile(@SessionAttribute("user") Users user, ModelAndView mav) {
        mav.addObject("listContent", getlistContent(user));
        mav.addObject("post", new Posts());
        mav.setViewName("profile");
        return mav;
    }
    
    @Transactional
    @RequestMapping(value = "deleteMedia", method = RequestMethod.POST)
    public String deleteMedia(@RequestParam(value = "mediaId") int mediaId) {
        for (CommentsMedia commentsMedia : commentsMediaService.list()) {
            if (commentsMedia.getMedia().getId() == mediaService.byId(mediaId).getId()) {
              commentsMediaService.delete(commentsMedia);
            }
        }
       mediaService.delete(mediaService.byId(mediaId));
        return "redirect:profile";
    }
    
    @Transactional
    @RequestMapping(value = "deletePost", method = RequestMethod.POST)
    public String deletePost(@RequestParam(value = "postId") int postId) {
            for (Comments comment : commentsService.list()) {
            if (comment.getPostsId().getId() == postsService.byId(postId).getId()) {
              commentsService.delete(comment);
            }
        }
        postsService.delete(postsService.byId(postId));
        return "redirect:profile";
    }
    
    @Transactional
    @RequestMapping(value = "addPost", method = RequestMethod.POST)
    public String addPost(@SessionAttribute("user") Users user, Model model,
            @ModelAttribute(value = "post") Posts post, BindingResult result) {
        
        postValidation.validate(post, result);
        if (result.hasErrors()) {
            model.addAttribute("listContent", getlistContent(user));
            return "profile";
        }
        
        post.setDateCreated(new Date());
        post.setUsersId(user);
        postsService.save(post);
        return "redirect:profile";
    }
    
    @RequestMapping(value = "/addContent", method = RequestMethod.POST)
    @Transactional
    public String uploadFile(@SessionAttribute("user") Users user, Model model,
            @ModelAttribute("uploadedFile") UploadedFile uploadedFile,
            BindingResult result) {
        
        String fileName = null;
        
        MultipartFile file = uploadedFile.getFile();
        
        contentValidation.validate(uploadedFile, result);
        
        if (result.hasErrors()) {
            model.addAttribute("listContent", getlistContent(user));
            return "profile";
        } else {
            
            try {
                
                Media media = new Media();
                byte[] bytes = file.getBytes();
                
                fileName = file.getOriginalFilename();
                
                String rootPath = System.getProperty("catalina.home");
                File dir = new File(rootPath + File.separator + "tmpFiles");
                
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                
                File loadFile = new File(dir.getAbsolutePath() + File.separator + fileName);
                
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(loadFile));
                stream.write(bytes);
                stream.flush();
                stream.close();
                
                if (contentValidation.isPicture(fileName)) {
                    javaxt.io.Image image = new javaxt.io.Image(dir.getAbsolutePath() + File.separator + fileName);
                    image.setHeight(250);
                    byte[] newBytes = image.getByteArray();
                    BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(loadFile));
                    stream1.write(newBytes);
                    stream1.flush();
                    stream1.close();
                    media.setMediaType(Media.MediaType.IMAGE);
                }
                if (contentValidation.isAudio(fileName)) {
                    media.setMediaType(Media.MediaType.AUDIO);
                }
                if (contentValidation.isVideo(fileName)) {
                    media.setMediaType(Media.MediaType.VIDEO);
                }
                
                media.setMedia("files" + File.separator + file.getOriginalFilename());
                media.setDateCreated(new Date());
                media.setUsersId(user);
                
                mediaService.save(media);
                
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "redirect:profile";
    }
    
    private List getlistContent(Users user) {
        Map<Date, Object> content = new TreeMap<>();
        List<Object> listContent = new ArrayList<>();
        if (user.getMediaList() != null) {
            user.getMediaList().clear();
        }
        for (Media media : mediaService.list()) {
            if (media.getUsersId().equals(user)) {
                content.put(media.getDateCreated(), media);
            }
        }
        if (user.getPostsList() != null) {
            user.getPostsList().clear();
        }
        for (Posts post : postsService.list()) {
            if (post.getUsersId().equals(user)) {
                content.put(post.getDateCreated(), post);
            }
        }
        for (Map.Entry<Date, Object> entry : content.entrySet()) {
            listContent.add(0, entry.getValue());
        }
        return listContent;
    }
    
    @RequestMapping(value = "/getTags", method = RequestMethod.GET)
    public @ResponseBody
    List<Tag> getTags(@RequestParam String tagName) {
        return simulateSearchResult(tagName);
    }
    
    private List<Tag> simulateSearchResult(String tagName) {
        
        List<Tag> result = new ArrayList<Tag>();
        List<Tag> data = new ArrayList<Tag>();
        
        for (Users user : userService.list()) {
            if (user.getRegstatus()) {
                data.add(new Tag(user));
            }
        }
        // iterate a list and filter by tagName
        for (Tag tag : data) {
            if (tag.getTagName().toLowerCase().contains(tagName.toLowerCase())) {
                result.add(tag);
            }
        }
        
        return result;
    }
    
    public class Tag {
        
        public int id;
        public String tagName;
        
        public int getId() {
            return id;
        }
        
        public void setId(int id) {
            this.id = id;
        }
        
        public String getTagName() {
            return tagName;
        }
        
        public void setTagName(String tagName) {
            this.tagName = tagName;
        }
        
        public Tag(Users user) {
            this.id = user.getId();
            this.tagName = user.getFirstname() + " " + user.getLastname();
        }
        
    }
}
