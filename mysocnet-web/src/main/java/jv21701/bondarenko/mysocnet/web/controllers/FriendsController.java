package jv21701.bondarenko.mysocnet.web.controllers;

import java.util.HashMap;
import java.util.Map;
import jv21701.bondarenko.mysocnet.entityes.Friends;
import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.mail.bean.Mail;
import jv21701.bondarenko.mysocnet.mail.service.MailService;
import jv21701.bondarenko.mysocnet.service.FriendsService;
import jv21701.bondarenko.mysocnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class FriendsController {

    @Autowired
    private UserService userService;

    @Autowired
    private FriendsService friendsService;

    @Autowired
    private MailService mailService;

    @Transactional
    @RequestMapping(value = "friends", method = RequestMethod.GET)
    public String foundUser(@SessionAttribute("user") Users user,
            ModelMap modelMap) {
        Map<Users, Integer> friendsMap = new HashMap<>();
        for (Friends friends : friendsService.list()) {
            if (friends.isStatus() && friends.getUsersId().equals(user)) {
                friendsMap.put(userService.byId(friends.getFriendid()), friends.getFriendid());
            }
            if (friends.isStatus() && userService.byId(friends.getFriendid()).equals(user)) {
                friendsMap.put(friends.getUsersId(), friends.getFriendid());
            }
        }
        modelMap.addAttribute("friendsMap", friendsMap);
        return "friends";
    }

    @Transactional
    @RequestMapping(value = "offerFriendship", method = RequestMethod.POST)
    public String getOfferFriendship(@SessionAttribute("user") Users user,
            @RequestParam("foundUserId") Integer foundUserId) {
        Friends friend = new Friends();
        friend.setFriendid(foundUserId);
        friend.setUsersId(user);
        friendsService.save(friend);
        Mail mail = new Mail();
        mail.setMailFrom("noreply@domain.com");
        mail.setMailTo(userService.byId(foundUserId).getLogin());
        mail.setMailSubject("Message from Mysocnet");
        mail.setMailContent("Your received a request for friendship from "
                + user.getFirstname() + " " + user.getLastname());
        mailService.sendEmail(mail);
        return "redirect:founduser";
    }

    @Transactional
    @RequestMapping(value = "argeeFrienhip", method = RequestMethod.POST)
    public String argeeFrienhip(@RequestParam("friendsId") Integer friendsId) {
        Friends friends = friendsService.byId(friendsId);
        friends.setStatus(true);
        friendsService.update(friends);
        Mail mail = new Mail();
        mail.setMailFrom("noreply@domain.com");
        System.out.println(friendsService.byId(friendsId).getUsersId().getLogin());
        mail.setMailTo(friendsService.byId(friendsId).getUsersId().getLogin());
        mail.setMailSubject("Message from Mysocnet");
        mail.setMailContent(userService.byId(friendsService.byId(friendsId).getFriendid()).getFirstname()
                + " " + userService.byId(friendsService.byId(friendsId).getFriendid()).getLastname()
                + " confirmed your friend request");
        mailService.sendEmail(mail);
        return "redirect:home";
    }

    @Transactional
    @RequestMapping(value = "disargeeFrienhip", method = RequestMethod.POST)
    public String disargeeFrienhip(@RequestParam("friendsId") Integer friendsId) {
        Mail mail = new Mail();
        mail.setMailFrom("noreply@domain.com");
        System.out.println(friendsService.byId(friendsId).getUsersId().getLogin());
        mail.setMailTo(friendsService.byId(friendsId).getUsersId().getLogin());
        mail.setMailSubject("Message from Mysocnet");
        mail.setMailContent(userService.byId(friendsService.byId(friendsId).getFriendid()).getFirstname()
                + " " + userService.byId(friendsService.byId(friendsId).getFriendid()).getLastname() 
                + " rejected your friend request");
        mailService.sendEmail(mail);
        friendsService.delete(friendsService.byId(friendsId));
        return "redirect:home";
    }
}
