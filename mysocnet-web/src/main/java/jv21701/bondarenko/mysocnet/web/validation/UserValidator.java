package jv21701.bondarenko.mysocnet.web.validation;

import jv21701.bondarenko.mysocnet.dto.UserDto;
import jv21701.bondarenko.mysocnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UserValidator implements Validator {

    private Pattern pattern;
    private Matcher matcher;

    @Autowired
    private UserService userService;

    private final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$";

    @Override
    public boolean supports(Class<?> clazz) {
        return UserDto.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {

        UserDto userDto = (UserDto) object;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstname", "user.firstname.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "user.lastname.empty");
        ValidationUtils.rejectIfEmpty(errors, "login", "user.email.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "user.password.empty");

        if (userService.byUsername(userDto.getLogin()) != null) {
            errors.rejectValue("login", "user.email.isexist");
        }

        if (!validateEmail(userDto.getLogin())) {
            errors.rejectValue("login", "user.email.valid");
        }
    }

    private boolean validateEmail(String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
