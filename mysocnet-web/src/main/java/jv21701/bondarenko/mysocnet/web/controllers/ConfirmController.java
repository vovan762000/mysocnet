package jv21701.bondarenko.mysocnet.web.controllers;

import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ConfirmController {
    
    @Autowired
    UserService userService;

    @RequestMapping(value = "/confirm", method = RequestMethod.GET)
    public String showConfirmationPage(@RequestParam("token") String token) {

        Users user = userService.getByVerificationToken(token);

        if (user == null) { 
            return "redirect:/registration";
        } else { 
            user.setRegstatus(true);
            userService.update(user);
            return "redirect:/login";
        }
    }
}
