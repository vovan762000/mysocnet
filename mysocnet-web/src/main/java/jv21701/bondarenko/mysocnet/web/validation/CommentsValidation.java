package jv21701.bondarenko.mysocnet.web.validation;

import jv21701.bondarenko.mysocnet.entityes.Comments;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class CommentsValidation implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        return Comments.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Comments comment = (Comments) o;
        if (comment.getComment().length() > 50) {
            errors.rejectValue("comment", "comment.valid.lenth");
        }
        if (comment.getComment() == null || comment.getComment().trim() == "") {
            errors.rejectValue("comment", "comment.valid.post");
        }
    }

}
