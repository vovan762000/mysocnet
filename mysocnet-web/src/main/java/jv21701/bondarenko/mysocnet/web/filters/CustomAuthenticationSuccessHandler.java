package jv21701.bondarenko.mysocnet.web.filters;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jv21701.bondarenko.mysocnet.entityes.Users;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationSuccessHandler implements
        AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
            HttpServletResponse response, Authentication authentication) throws IOException,
            ServletException {
        String targetUrl = determineTargetUrl();
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    protected String determineTargetUrl() {
        
        Users authUser = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try{
        if (authUser!=null && authUser.getRegstatus()) {
            return "home";
        }else
        throw new UsernameNotFoundException("user not found");
        }catch (Exception ex){
        return "login?error";
        }
    }
}
