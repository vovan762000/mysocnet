<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fmt"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>
<html>
    <head>
        <style>
            .banned {
                color: #FF4500;
                font-style: italic;
                font-weight: bold;
            }
        </style>
        <link href="static/css/bootstrap.min.css" rel="stylesheet">
        <link href="static/css/sb-admin.css" rel="stylesheet">
        <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page-wrapper">
            <div class="container-fluid">             
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Users</h2>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Username</th>
                                            <th>Firstname</th>
                                            <th>Lastname</th>
                                            <th>Message</th>
                                            <th>Banning user</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="users" items="${users}" varStatus="number">
                                            <c:if test="${users.id ne user.id}">
                                            <tr>
                                                <c:choose>
                                                    <c:when test="${users.regstatus}">
                                                        <td>${number.count}</td>
                                                        <td>${users.login}</td>
                                                        <td>${users.firstname}</td>
                                                        <td>${users.lastname}</td>
                                                        <td><a href="correspondence?id=${users.id}" class="btn btn-warning">Send message</a></td>
                                                        <form:form action="banUser" method="POST">
                                                            <td><button type="submit" class="btn btn-danger" name="banUser" value="${users.id}">Ban user</button></td>
                                                        </form:form>
                                                    </c:when>
                                                    <c:when test="${users.regstatus == false}">
                                                        <td>${number.count}</td>
                                                        <td class="banned">${users.login}</td>
                                                        <td class="banned">${users.firstname}</td>
                                                        <td class="banned">${users.lastname}</td>
                                                        <td></td>
                                                        <form:form action="unBanUser" method="POST">
                                                            <td><button type="submit" class="btn btn-success" name="unBanUser" value="${users.id}">Unban user</button></td>
                                                        </form:form>
                                                    </c:when>
                                                </c:choose>
                                            </tr>
                                            </c:if>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </sec:authorize>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<%@include file="../commons/footer.jsp" %>
