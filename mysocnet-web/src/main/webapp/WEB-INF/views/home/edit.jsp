<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>
<html
<head>
    <style>
        .error {
            color: #ff0000;
            font-style: italic;
            font-weight: bold;
        }
    </style>
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <link href="static/css/sb-admin.css" rel="stylesheet">

    <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
    <div id="page-wrapper">
        <div class="container-fluid">             
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <form:form  method="POST" action="">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2">Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Username</td>
                                        <td>${user.login}</td>
                                    </tr>
                                    <tr>
                                        <td>Firstname</td>
                                        <td>
                                            <input class="form-control" type="text" name="firstname" value="${user.firstname}"/>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lastname</td>
                                        <td>
                                            <input class="form-control" type="text" name="lastname" value="${user.lastname}"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <input class="btn btn-success" type="submit" value="Save and Relogin"/>
                        </form:form>
                        <br/>
                        <form:form method="POST" enctype="multipart/form-data" modelAttribute="uploadAvatar" action="uploadAvatar">
                            <div class="form-group">
                                <label>Or upload avatar</label>
                                <input type="file" name="file">
                                <form:errors path="file" cssClass="error"></form:errors>
                                </div>
                                <input class="btn btn-success" type="submit" value="Upload Avatar and Relogin">
                        </form:form> 
                        <br/>  

                    </div>
                </div>
            </div>
        </div>
    </div>
<body>
</html>
<%@include file="../commons/footer.jsp" %>

