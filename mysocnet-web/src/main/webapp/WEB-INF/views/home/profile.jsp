<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>
<html>
    <head>
        <style>
            .error {
                color: #ff0000;
                font-style: italic;
                font-weight: bold;
            }
        </style>
        <link href="static/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="static/css/sb-admin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <script src="static/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="static/js/bootstrap.min.js"></script>
        <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="<c:url value="static/js/jquery.1.10.2.min.js" />"></script>
        <script src="<c:url value="static/js/jquery.autocomplete.min.js" />"></script>
        <link href="<c:url value="static/css/main.css" />" rel="stylesheet">
    </head>
    <body>
        <div id="page-wrapper">

            <div class="container-fluid"> 
                <form:form action="founduser" method="POST">
                    <div class="form-group">
                        <input type="text" name="foundUser" id="w-input-search" size="40 class="form-control" placeholder="Enter searching user" >
                               <span>
                            <button id="button-id" type="submit">Search</button>
                        </span>
                    </div>
                </form:form>
                <script>
                    $(document).ready(function () {

                        $('#w-input-search').autocomplete({
                            serviceUrl: '${pageContext.request.contextPath}/getTags',
                            paramName: "tagName",
                            delimiter: ",",
                            transformResult: function (response) {

                                return {
                                    //must convert json to javascript object before process
                                    suggestions: $.map($.parseJSON(response), function (item) {

                                        return {value: item.tagName, data: item.id};
                                    })

                                };

                            }

                        });

                    });
                </script>
                <br/> 
                <form:form modelAttribute="post" action="addPost" method="POST">
                    <div class="form-group" >
                        <input type="text" name="post" class="form-control"  placeholder="Enter your post here" >
                        <form:errors path="post" cssClass="error"></form:errors>
                        </div>
                        <input class="btn btn-success" type="submit" value="Publish" name="Save/">
                </form:form>
                <br><br>
                <form:form method="POST" enctype="multipart/form-data" modelAttribute="uploadedFile" action="addContent">
                    <div class="form-group">
                        <label>Or add content max 10 Gb</label>
                        <input type="file" name="file">
                        <form:errors path="file" cssClass="error"></form:errors>
                        </div>
                        <input class="btn btn-success" type="submit" value="Publish" name="Save/">
                </form:form>  

                <div class="row">
                    <div class="col-lg-12">
                        <h2>My Dashboard</h2>
                        <div class="table-responsive">
                            <table class="table table-hover table-hover">
                                <tbody>
                                    <c:set var="posts" value="${user.postsList}" />
                                    <c:set var="media" value="${user.mediaList}" />

                                    <c:forEach var="listContent" items="${listContent}">
                                        <c:choose>
                                            <c:when test="${listContent.getClass().name == 'jv21701.bondarenko.mysocnet.entityes.Media'}">
                                                <c:choose>
                                                    <c:when test="${listContent.mediaType == 'IMAGE'}">
                                                        <tr>
                                                            <td><img class="img-thumbnail" src="${listContent.media}" alt="">
                                                                <br/> 
                                                                <c:out value="${user.firstname}" />&nbsp<c:out value="${user.lastname}" />
                                                                <c:out value="${listContent.dateCreated}" />
                                                                <form:form action="deleteMedia" method="POST" >
                                                                    <button name="mediaId" value="${listContent.id}"
                                                                            type="submit" class="btn btn-xs btn-danger">Delete</button>
                                                                </form:form> 
                                                            </td>                                  
                                                        </tr>
                                                    </c:when>
                                                    <c:when test="${listContent.mediaType == 'VIDEO'}">
                                                        <tr>
                                                            <td>
                                                                <video width="320" height="240" controls>
                                                                    <source src="${listContent.media}" type="video/mp4">
                                                                </video>
                                                                <br/> 
                                                                <c:out value="${user.firstname}" />&nbsp<c:out value="${user.lastname}" />
                                                                <c:out value="${listContent.dateCreated}" />
                                                                <form:form action="deleteMedia" method="POST" >
                                                                    <button name="mediaId" value="${listContent.id}"
                                                                            type="submit" class="btn btn-xs btn-danger">Delete</button>
                                                                </form:form> 
                                                            </td>                                  
                                                        </tr>
                                                    </c:when>
                                                    <c:when test="${listContent.mediaType == 'AUDIO'}">
                                                        <tr>
                                                            <td>
                                                                <audio controls>
                                                                    <source src="${listContent.media}" type="audio/mpeg">
                                                                </audio>
                                                                <br/> 
                                                                <c:out value="${user.firstname}" />&nbsp<c:out value="${user.lastname}" />
                                                                <c:out value="${listContent.dateCreated}" />
                                                                <form:form action="deleteMedia" method="POST" >
                                                                    <button name="mediaId" value="${listContent.id}"
                                                                            type="submit" class="btn btn-xs btn-danger">Delete</button>
                                                                </form:form> 
                                                            </td>                                  
                                                        </tr>
                                                    </c:when>
                                                </c:choose>
                                            </c:when>
                                            <c:when test="${listContent.getClass().name == 'jv21701.bondarenko.mysocnet.entityes.Posts'}">
                                                <tr>
                                                    <td><c:out value="${listContent.post}" />
                                                        <br/> 
                                                        <c:out value="${user.firstname}" />&nbsp<c:out value="${user.lastname}" />
                                                        <c:out value="${listContent.dateCreated}" />
                                                        <form:form action="deletePost" method="POST" >
                                                            <button name="postId" value="${listContent.id}"
                                                                    type="submit" class="btn btn-xs btn-danger">Delete</button>
                                                        </form:form> 
                                                    </td>                                  
                                                </tr>
                                            </c:when>  
                                        </c:choose>  
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </body> 
</html>