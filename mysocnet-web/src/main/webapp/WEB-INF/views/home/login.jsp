<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Bootstrap Snippet: Login Form</title>

        <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>

        <link rel="stylesheet" href="static/css/login.css">


    </head>

    <body>
        <div class="wrapper">
            <form:form class="form-signin" method="POST" modelAttribute="user" action="">       
                <h2 class="form-signin-heading">Please login</h2>
                <input type="email" class="form-control" name="login" placeholder="Email Address" required="" autofocus="" />
                <p></p>
                <input type="password" class="form-control" name="password" placeholder="Password" required=""/> 
                <span ><font color="red">${error}</font></span>
                <label class="checkbox">
                    <input type="checkbox" value="true" id="rememberMe" name="remember-me"> Remember me
                </label>             
                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                <p></p>
                <a href="${contextPath}/registration">Create an account</a>
            </form:form>

        </div>


    </body>
</html>
