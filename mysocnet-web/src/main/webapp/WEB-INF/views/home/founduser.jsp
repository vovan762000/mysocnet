<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>
<html>
    <head>

        <link href="static/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="static/css/sb-admin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page-wrapper">
            <div class="container-fluid"> 
                <c:forEach var="foundUser" items="${foundUsers}">
                    <img class="img-thumbnail" src="${foundUser.key.avatar}" alt="">
                    <span style="font-size: 30px; line-height: 18px;"> <c:out value="${foundUser.key.firstname}"/> </span>
                    <span style="font-size: 30px; line-height: 18px;"><c:out value="${foundUser.key.lastname}"/> </span>
                    <a href="correspondence?id=${foundUser.key.id}" class="btn btn-success">Send message</a>                   
                    <c:if test="${foundUser.value == false}">
                        <form:form action="offerFriendship" method="POST">
                            <button name="foundUserId" value="${foundUser.key.id}"
                                    type="submit" class="btn btn-xs btn-success">Offer Friendship</button>
                        </form:form>
                    </c:if>
                    <br> 
                    <br> 
                </c:forEach>
            </div>
        </div>
    </body>
</html>
<%@include file="../commons/footer.jsp" %>