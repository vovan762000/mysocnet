<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
 
    <title></title>
 
    <!-- Bootstrap core CSS -->
    <link href="<c:url value="static/css/index.css" />" rel="stylesheet">
 
    <!-- Custom styles for this template -->
    <link href="<c:url value="static/css/indexcustom.css" />" rel="stylesheet">

</head>
 
<body>
 
<div class="container">
 
    <div class="jumbotron" style="margin-top: 20px;">
        <h1>Mysocnet</h1>
        <p class="lead">
            Mysocnet - this is simple social network.
        </p>
            <p><a class="btn btn-lg btn-success" href="<c:url value="/login" />" role="button">Sign up</a></p>
            <p><a class="btn btn-lg btn-success" href="<c:url value="/registration" />" role="button">Registration</a></p>
            <a href="/description">Description</a>
    </div>
 
    <div class="footer">
        <p>© Vladimir Bondarenko 2018</p>
    </div>
 
</div>
</body>
</html>
