<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>
<html>
    <head>
        <style>
            .message1 {
                color: #00008B;
                font-style: italic;
                font-weight: bold;
            }
        </style>
        <style>
            .message2 {
                color: #FF4500;
                font-style: italic;
                font-weight: bold;
            }
        </style>
        <link href="static/css/bootstrap.min.css" rel="stylesheet">
        <link href="static/css/sb-admin.css" rel="stylesheet">
        <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page-wrapper">
            <h2>Friends</h2>
            <div class="container-fluid"> 
                <c:forEach var="friendsMap" items="${friendsMap}"> 
                        <img class="img-thumbnail" src="${friendsMap.key.avatar}" alt="">
                        <span style="font-size: 30px; line-height: 18px;"> <c:out value="${friendsMap.key.firstname}"/> </span>
                        <span style="font-size: 30px; line-height: 18px;"><c:out value="${friendsMap.key.lastname}"/> </span>                
                        <a href="correspondence?id=${friendsMap.key.id}" class="btn btn-success">Send message</a>
                        <br>
                </c:forEach>
            </div>
        </div>
    </body>
</html>
<%@include file="../commons/footer.jsp" %>
