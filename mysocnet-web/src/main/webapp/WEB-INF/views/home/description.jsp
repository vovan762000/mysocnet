<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
 
    <title></title>
 
    <!-- Bootstrap core CSS -->
    <link href="<c:url value="static/css/index.css" />" rel="stylesheet">
 
    <!-- Custom styles for this template -->
    <link href="<c:url value="static/css/indexcustom.css" />" rel="stylesheet">

</head>
 
<body>
 
<div class="container">
 
    <div class="jumbotron" style="margin-top: 20px;">
        <h1>Mysocnet</h1>
        <p>Basic Components and Usage Rules</p>
        <br>
        <p class="lead">
            <i>The registration or authorization page includes the following components:</i><p></p>
        <i>1.General form for authorization or registration:</i>
        <p>Authorization form</p>
        <p>-The form of authorization includes - entering the login and password;</p>
        <p>-Login is entered in the format of email;</p>
        <p>-Password at least 6 characters with Latin letters and numbers;</p>
        <p>-Introduced data must be validated, with incorrect data - an error message;</p>
        <p>2.Button for authorization confirmation</p>
        <p>-when you click on the button with the correct data - go to the home page, if incorrect - an error message;</p>
        <p>3.Button to switch to registration</p>
        <p>-press clicking on the registration page;</p>
        <p>Registration form</p>
        <p>1.form for entering login, password, name, surname;</p>
        <p>-data input must be validated</p>
        <i>2.button to confirm registration</i>
        <p>-when pressed with incorrect data - an error message;</p>
        <p>-if correct - sending a confirmation letter and a link, going to the page confirming the successful registration;undefined - when clicking on a link in a letter - the registration is performed and the page goes to the authorization page;</p>
        <i>The home page includes the following:</i>
        <p>1.The user's wall with a set of posts of users in chronological order-records on the wall can be in both text format and media files</p>
        <p>-Player has the ability to add likes to posts of other users</p>
        <p>- the user has the opportunity to add comments to the posts of other users</p>
        <p>-(when authorizing a user with ADMIN rights), there is a DELETE button under each post that allows you to delete this post;</p>
        <p>-all input fields pass validation;</p>
        <i>2.The name and surname of the user in the upper left corner, which is also a button to return to the main page</i><p></p>
        <i>3.Knoka Friends, when clicked on it occurs on the page with a list of friends of the user.</i><p></p>
        <i>4. The icon with a drop-down list in the upper right corner includes:</i><p></p>
        <i>4.1 HOME button:</i>
        <p>-when pressed, go to the home page;</p>
        <i>4.2 Profile button: when pressed, the undefined go to the User Profile page, which includes:</i>
        <p>-field search for other users registered on the site. When you enter letters in the search field when the letters match in the name and last name of registered users, a drop-down list appears with prompts from which you can select the right user and when you click on it, enter it in the input field;</p>
        <p>-When you click on the Search button, you go to the page with the name and surname of the users found and their avatar. Under the name and surname there is a button for sending a request for friendship to this user (when the request is submitted or if the user is another button is missing). Left of the avatar is the button Sending a message, when clicked, the page of the message is sent to the user;</p>
        <i>4.3 the Change button</i>
        <p>When clicked, the user goes to the user profile re-username page, where you can change:</p>
        <p>-the name and surname of the user;</p>
        <p>-Avatar user;</p>
        <p>When changing undefined avatar or user name is re-logged;</p>
        <i>4.4 Logout button-when clicked, a logging occurs;</i><p>
        <i>5.button OFFERS FRIENDSHIP with the drop-down menu:</i>
        <p>-with existing requests for friendship, the names of the users who submitted the request and two buttons with the request to confirm or reject the friendship will be displayed in the drop-down menu. Upon confirmation, the user becomes a friend and placed in the FRIENDS list, which is sent a notification letter. When you click on reject, The user who submitted the request that his request was rejected;</p>
        <i>6.Avatar of the user.</i><p></p>
        <i>7. Users button (when authorizing a user with ADMIN rights)</i>
        <p>-Press the button to go to the page with a list of all users registered on the site;</p>
        <p>-for each user there is a button Sent Message and button Ban;</p>
        <p>-by clicking the Sent Message button ADMIN goes to the page for sending the message of the specific user;</p>
        <p>-by clicking on the Ban button, a specific account user account is logged in with undefined sending a letter to his mail that his account was blocked. After that, in the place of the Ban button, the Unban button appears, when you click on which the account of a particular user is unlocked.<p>
            <br>
            <i>All input fields on the site are validated!</i>
            <br>
        </p>
           <a href="/index">Back</a>
    </div>
 
    <div class="footer">
        <p>© Vladimir Bondarenko 2018</p>
    </div>
 
</div>
</body>
</html>
