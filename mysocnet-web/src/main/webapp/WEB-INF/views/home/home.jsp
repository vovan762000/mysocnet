<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>
<head>
    <style>
        .comment {
            color: #00008B;
            font-style: italic;
            font-weight: bold;
        }
        .error {
            color: #ff0000;
            font-style: italic;
            font-weight: bold;
        }
    </style>
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<div id="page-wrapper">
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <h2>Dashboard</h2>
                <div class="table-responsive">
                    <table class="table table-hover table-hover">
                        <tbody>
                            <c:forEach var="content" items="${content}">
                                <c:choose>
                                    <c:when test="${content.getClass().name == 'jv21701.bondarenko.mysocnet.entityes.Media'}">
                                        <c:choose>
                                            <c:when test="${content.mediaType == 'IMAGE'}">
                                                <tr>
                                                    <td><img class="img-thumbnail" src="${content.media}" alt="">
                                                        <br/> 
                                                        <form:form action="addLikeMedia" method="POST">
                                                            <c:out value="${content.usersId.firstname}" />&nbsp<c:out value="${content.usersId.lastname}" />
                                                            <c:out value="${content.dateCreated}" />                                                                                                        
                                                            <button name="mediaid" value="${content.id}"
                                                                    type="submit" class="btn btn-xs btn-success">Like &nbsp ${content.likeCount}</button>       
                                                        </form:form>
                                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                            <form:form action="deleteMedia" method="POST" >
                                                                <button name="mediaId" value="${content.id}"
                                                                        type="submit" class="btn btn-xs btn-danger">Delete</button>
                                                            </form:form> 
                                                        </sec:authorize>
                                                        <c:forEach var="commentsMedia" items="${commentsMedia}">
                                                            <c:if test="${commentsMedia.media.media == content.media}">
                                                                <br><i class="comment"><c:out value="${commentsMedia.commentsMedia}"/></i><br>
                                                                <i class="comment"><c:out value="${commentsMedia.userComName}"/></i>
                                                                <i class="comment"><c:out value="${commentsMedia.dateCreated}"/></i>
                                                            </c:if>
                                                        </c:forEach>
                                                        <br/>
                                                        <form:form modelAttribute="commentMedia" action="addCommentsMedia" method="POST" >
                                                            <c:if test="${user.id ne content.usersId.id}">
                                                                <div class="form-group">
                                                                    <input type="text" name="commentsMedia" 
                                                                           class="form-control" placeholder="Enter your comment here"/>
                                                                    <form:errors path="commentsMedia" cssClass="error"></form:errors>
                                                                    <button name="postid" value="${content.id}"
                                                                            type="submit" class="btn btn-xs btn-success">add</button>
                                                                </div>
                                                            </c:if>
                                                        </form:form>
                                                    </td>                                  
                                                </tr>
                                            </c:when>
                                            <c:when test="${content.mediaType == 'VIDEO'}">
                                                <tr>
                                                    <td>
                                                        <video width="320" height="240" controls>
                                                            <source src="${content.media}" type="video/mp4">
                                                        </video>
                                                        <br/> 
                                                        <form:form action="addLikeMedia" method="POST">
                                                            <c:out value="${content.usersId.firstname}" />&nbsp<c:out value="${content.usersId.lastname}" />
                                                            <c:out value="${content.dateCreated}" />                                                                                                       
                                                            <button name="mediaid" value="${content.id}"
                                                                    type="submit" class="btn btn-xs btn-success">Like &nbsp ${content.likeCount}</button>       
                                                        </form:form>
                                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                            <form:form action="deleteMedia" method="POST" >
                                                                <button name="mediaId" value="${content.id}"
                                                                        type="submit" class="btn btn-xs btn-danger">Delete</button>
                                                            </form:form> 
                                                        </sec:authorize>
                                                        <c:forEach var="commentsMedia" items="${commentsMedia}">
                                                            <c:if test="${commentsMedia.media.media == content.media}">
                                                                <br><i class="comment"><c:out value="${commentsMedia.commentsMedia}"/></i><br>
                                                                <i class="comment"><c:out value="${commentsMedia.userComName}"/></i>
                                                                <i class="comment"><c:out value="${commentsMedia.dateCreated}"/></i>
                                                            </c:if>
                                                        </c:forEach>
                                                        <br/>
                                                        <form:form modelAttribute="commentMedia" action="addCommentsMedia" method="POST" >
                                                            <c:if test="${user.id ne content.usersId.id}">
                                                                <div class="form-group">
                                                                    <input type="text" name="commentsMedia" 
                                                                           class="form-control" placeholder="Enter your comment here"/>
                                                                    <form:errors path="commentsMedia" cssClass="error"></form:errors>
                                                                    <button name="postid" value="${content.id}"
                                                                            type="submit" class="btn btn-xs btn-success">add</button>
                                                                </div>
                                                            </c:if>
                                                        </form:form>
                                                    </td>                                  
                                                </tr>
                                            </c:when>
                                            <c:when test="${content.mediaType == 'AUDIO'}">
                                                <tr>
                                                    <td>
                                                        <audio controls>
                                                            <source src="${content.media}" type="audio/mpeg">
                                                        </audio>
                                                        <br/> 
                                                        <form:form action="addLikeMedia" method="POST">
                                                            <c:out value="${content.usersId.firstname}" />&nbsp<c:out value="${content.usersId.lastname}" />
                                                            <c:out value="${content.dateCreated}" />                                                                                           
                                                            <button name="mediaid" value="${content.id}"
                                                                    type="submit" class="btn btn-xs btn-success">Like &nbsp ${content.likeCount}</button>       
                                                        </form:form>
                                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                            <form:form action="deleteMedia" method="POST" >
                                                                <button name="mediaId" value="${content.id}"
                                                                        type="submit" class="btn btn-xs btn-danger">Delete</button>
                                                            </form:form> 
                                                        </sec:authorize>
                                                        <c:forEach var="commentsMedia" items="${commentsMedia}">
                                                            <c:if test="${commentsMedia.media.media == content.media}">
                                                                <br><i class="comment"><c:out value="${commentsMedia.commentsMedia}"/></i><br>
                                                                <i class="comment"><c:out value="${commentsMedia.userComName}"/></i>
                                                                <i class="comment"><c:out value="${commentsMedia.dateCreated}"/></i>
                                                            </c:if>
                                                        </c:forEach>
                                                        <br/>
                                                        <form:form modelAttribute="commentMedia" action="addCommentsMedia" method="POST" >
                                                            <c:if test="${user.id ne content.usersId.id}">
                                                                <div class="form-group">
                                                                    <input type="text" name="commentsMedia" 
                                                                           class="form-control" placeholder="Enter your comment here"/>
                                                                    <form:errors path="commentsMedia" cssClass="error"></form:errors>
                                                                    <button name="postid" value="${content.id}"
                                                                            type="submit" class="btn btn-xs btn-success">add</button>
                                                                </div>
                                                            </c:if>
                                                        </form:form>
                                                    </td>                                  
                                                </tr>
                                            </c:when>
                                        </c:choose>
                                    </c:when>
                                    <c:when test="${content.getClass().name == 'jv21701.bondarenko.mysocnet.entityes.Posts'}">
                                        <tr>
                                            <td><c:out value="${content.post}" />
                                                <br/> 
                                                <form:form action="addLikePost" method="POST">
                                                    <c:out value="${content.usersId.firstname}" />&nbsp<c:out value="${content.usersId.lastname}" />
                                                    <c:out value="${content.dateCreated}" />

                                                    <button name="postid" value="${content.id}"
                                                            type="submit" class="btn btn-xs btn-success">Like &nbsp ${content.likeCount}</button>       
                                                </form:form>
                                                <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                    <form:form action="deletePost" method="POST" >
                                                        <button name="postId" value="${content.id}"
                                                                type="submit" class="btn btn-xs btn-danger">Delete</button>
                                                    </form:form>
                                                </sec:authorize>
                                                <c:forEach var="comments" items="${comments}">
                                                    <c:if test="${comments.postsId.post == content.post}">
                                                        <br><i class="comment"><c:out value="${comments.comment}"/></i><br>
                                                        <i class="comment"><c:out value="${comments.userComName}"/></i>
                                                        <i class="comment"><c:out value="${comments.dateCreated}"/></i>
                                                    </c:if>
                                                </c:forEach>
                                                <br/>
                                                <form:form modelAttribute="comment" action="addComment" method="POST" >
                                                    <c:if test="${user.id ne content.usersId.id}">
                                                        <div class="form-group">
                                                            <input type="text" name="comment" 
                                                                   class="form-control" placeholder="Enter your comment here"/>
                                                            <form:errors path="comment" cssClass="error"></form:errors>
                                                            <button name="postid" value="${content.id}"
                                                                    type="submit" class="btn btn-xs btn-success">add</button>
                                                        </div>
                                                    </c:if>
                                                </form:form>
                <!--                                    <fofm id="content" value="${content.usersId}">
                                                <button id="but"
                                                        class="btn btn-xs btn-success"><b>Like &nbsp ${content.likeCount}</b>
                                                </button>       
                                                </form>-->
                                            </td>                                  
                                        </tr>
                                    </c:when>        
                                </c:choose>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../commons/footer.jsp" %>