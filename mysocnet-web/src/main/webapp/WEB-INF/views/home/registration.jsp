<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Bootstrap Snippet: Login Form</title>

        <style>
            .error {
                color: #ff0000;
                font-style: italic;
                font-weight: bold;
            }
        </style>
        <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>

        <link rel="stylesheet" href="static/css/login.css">


    </head>

    <body>
        <div class="wrapper">
            <form:form class="form-signin" method="POST" modelAttribute="userForm" action="">       
                <h2 class="form-signin-heading">Create your account</h2>
                <form:input class="form-control" path="firstname" type="text"
                            name="firstname" placeholder="Firstname" required="" autofocus=""/>
                <form:errors path="firstname" cssClass="error"></form:errors>
                    <p></p>
                <form:input class="form-control" path="lastname" type="text"
                            name="lastname" placeholder="Lastname" required=""/>
                <form:errors path="lastname" cssClass="error"></form:errors>
                    <p></p>
                <form:input type="text" class="form-control" path="login" placeholder="Email Address" required="" />
                <form:errors path="login" cssClass="error"></form:errors>
                    <p></p>
                <form:input type="password" class="form-control" path="password" placeholder="Password" required="" />
                <form:errors path="password" cssClass="error"></form:errors>

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
                    <p></p>
                    <a href="${contextPath}/login">Already registered?</a>
            </form:form>
        </div>


    </body>
</html>