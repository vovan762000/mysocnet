<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>
<html>
    <head>
        <style>
            .message1 {
                color: #00008B;
                font-style: italic;
                font-weight: bold;
            }
            .error {
                color: #ff0000;
                font-style: italic;
                font-weight: bold;
            }
            .message2 {
                color: #FF4500;
                font-style: italic;
                font-weight: bold;
            }
        </style>

        <link href="static/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="static/css/sb-admin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page-wrapper">
            <div class="container-fluid"> 
                <img class="img-thumbnail" src="${foundUser.avatar}" alt="">
                <span style="font-size: 30px; line-height: 18px;"> <c:out value="${foundUser.firstname}"/> </span>
                <span style="font-size: 30px; line-height: 18px;"><c:out value="${foundUser.lastname}"/> </span>
                <form:form modelAttribute="message" action="sendMessage" method="POST">
                    <div class="form-group" >
                        <input type="text" name="inMessage" class="form-control"  placeholder="Enter your message here" >
                        <form:errors path="inMessage" cssClass="error"></form:errors>
                        </div>
                        <button id="button-id" type="submit" name="foundUser" value="${foundUser.id}">Send message</button>
                </form:form>
                <c:forEach var="messList" items="${messList}">                    
                    <c:choose>
                        <c:when test="${messList.usersId.id == user.id}">
                            <p class="message1"><c:out value="${messList.inMessage}"/></p>
                            <p class="message1"><c:out value="${messList.dateCreated}"/></p>
                        </c:when>
                        <c:when test="${messList.usersId.id == foundUser.id}">
                            <p class="message2"><c:out value="${messList.inMessage}"/></p>
                            <p class="message2"><c:out value="${messList.dateCreated}"/></p>
                        </c:when>
                    </c:choose>
                </c:forEach>
            </div>
        </div>
    </body>
</html>
<%@include file="../commons/footer.jsp" %>