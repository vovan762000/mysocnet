
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <li>
            <img class="img-thumbnail" src="${user.avatar}" alt="">
            <sec:authorize access="hasRole('ROLE_ADMIN')">
            <a href="/usertable"><i class="fa fa-fw fa-dashboard"></i>Users</a>
            </sec:authorize>
            <a href="/friends"><i class="fa fa-fw fa-dashboard"></i>Friends</a>
        </li>
    </ul>
</div>
<!-- /.navbar-collapse -->
</nav>
