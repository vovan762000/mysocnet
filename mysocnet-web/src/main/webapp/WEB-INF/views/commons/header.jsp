<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <link href="static/css/bootstrap.min.css" rel="stylesheet">

        <link href="static/css/sb-admin.css" rel="stylesheet">

        <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    </head>

    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home">${user.firstname}&nbsp${user.lastname}</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown"><i>Offers friendship</i><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <c:forEach var="friends" items="${friends}">
                                <c:if test="${(friends.friendid == user.id) && (friends.status == false)}">
                                    <li>                          
                                        <a>${friends.usersId.firstname}&nbsp${friends.usersId.lastname}</a>
                                        <div>
                                            <div style="float: left;">
                                                <form:form action="argeeFrienhip" method="POST" >
                                                    <button name="friendsId" value="${friends.id}" type="submit" 
                                                            class="btn btn-xs btn-success">agree</button>
                                                </form:form>
                                            </div>
                                            <div style="float: right;">
                                                <form:form action="disargeeFrienhip" method="POST" >
                                                    <button name="friendsId" value="${friends.id}" 
                                                            type="submit" class="btn btn-xs btn-danger">disagree</button>
                                                </form:form>  
                                            </div>
                                        </div>
                                    </li>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ${firstname} &nbsp ${lastname} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="home"> Home</a>
                            </li>
                            <li>
                                <a href="profile"> Profile</a>
                            </li>
                            <li>
                                <a href="edit"> Change</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                </body>