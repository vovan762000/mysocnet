
package jv21701.bondarenko.mysocnet.daoimpl;

import jv21701.bondarenko.mysocnet.dao.AbstractDao;
import jv21701.bondarenko.mysocnet.dao.MediaDao;
import jv21701.bondarenko.mysocnet.entityes.Media;
import org.springframework.stereotype.Component;

@Component
public class MediaDaoImpl extends AbstractDao<Media, Integer> implements MediaDao{
    
}
