
package jv21701.bondarenko.mysocnet.daoimpl;

import jv21701.bondarenko.mysocnet.dao.AbstractDao;
import jv21701.bondarenko.mysocnet.dao.CommentsDao;
import jv21701.bondarenko.mysocnet.entityes.Comments;
import org.springframework.stereotype.Component;

@Component
public class CommentsDaoImpl extends AbstractDao<Comments, Integer> implements CommentsDao{
    
}
