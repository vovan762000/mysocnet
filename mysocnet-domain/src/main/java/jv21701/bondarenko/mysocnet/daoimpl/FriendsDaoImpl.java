
package jv21701.bondarenko.mysocnet.daoimpl;

import jv21701.bondarenko.mysocnet.dao.AbstractDao;
import jv21701.bondarenko.mysocnet.dao.FriendsDao;
import jv21701.bondarenko.mysocnet.entityes.Friends;
import org.springframework.stereotype.Component;

@Component
public class FriendsDaoImpl extends AbstractDao<Friends, Integer> implements FriendsDao{
    
}
