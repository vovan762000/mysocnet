
package jv21701.bondarenko.mysocnet.daoimpl;

import jv21701.bondarenko.mysocnet.dao.AbstractDao;
import jv21701.bondarenko.mysocnet.dao.UserLikePostDao;
import jv21701.bondarenko.mysocnet.entityes.UserLikePost;
import org.springframework.stereotype.Component;

@Component
public class UserLikePostDaoImpl extends AbstractDao<UserLikePost, Integer> implements UserLikePostDao{
    
}
