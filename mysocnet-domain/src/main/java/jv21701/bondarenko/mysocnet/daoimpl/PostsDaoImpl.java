
package jv21701.bondarenko.mysocnet.daoimpl;


import jv21701.bondarenko.mysocnet.dao.AbstractDao;
import jv21701.bondarenko.mysocnet.dao.PostsDao;
import jv21701.bondarenko.mysocnet.entityes.Posts;
import org.springframework.stereotype.Component;

@Component
public class PostsDaoImpl extends AbstractDao<Posts, Integer> implements PostsDao{
    
}
