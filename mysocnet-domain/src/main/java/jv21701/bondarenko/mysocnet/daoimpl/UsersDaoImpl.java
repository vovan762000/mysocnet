package jv21701.bondarenko.mysocnet.daoimpl;

import java.util.List;
import jv21701.bondarenko.mysocnet.dao.AbstractDao;
import jv21701.bondarenko.mysocnet.dao.UsersDao;
import jv21701.bondarenko.mysocnet.entityes.Users;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Component;

@Component
public class UsersDaoImpl extends AbstractDao<Users, Integer> implements UsersDao {

    @Override
    public int getUsersCount() {
        return (Integer) getCurrentSession()
                .createSQLQuery("select count(*) as counter from users")
                .addScalar("counter", StandardBasicTypes.INTEGER)
                .uniqueResult();
    }

    @Override
    public List<Users> getUsers(int offset, int limit) {
        return getCurrentSession().createSQLQuery("select * from users limit :offset, :limit")
                .addEntity(Users.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .list();
    }

    @Override
    public Users getByUsername(String login) {
        List<Users> users = getCurrentSession().createSQLQuery("select * from users")
                .addEntity(Users.class).list();
        for (Users user : users) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public Users byUsername(String username) {
        return (Users) getCurrentSession().createCriteria(Users.class)
                .add(Restrictions.eq("login", username)).uniqueResult();
    }

    @Override
    public Users getByVerificationToken(String regcode) {
        return (Users) getCurrentSession().createCriteria(Users.class)
                .add(Restrictions.eq("regcode", regcode)).uniqueResult();
    }
}
