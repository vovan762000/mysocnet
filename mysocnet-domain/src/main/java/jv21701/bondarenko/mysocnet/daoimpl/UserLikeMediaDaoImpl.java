
package jv21701.bondarenko.mysocnet.daoimpl;


import jv21701.bondarenko.mysocnet.dao.AbstractDao;
import jv21701.bondarenko.mysocnet.dao.UserLikeMediaDao;
import jv21701.bondarenko.mysocnet.entityes.UserLikeMedia;
import org.springframework.stereotype.Component;

@Component
public class UserLikeMediaDaoImpl extends AbstractDao<UserLikeMedia, Integer> implements UserLikeMediaDao{
    
}
