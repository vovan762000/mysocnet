
package jv21701.bondarenko.mysocnet.daoimpl;

import jv21701.bondarenko.mysocnet.dao.AbstractDao;
import jv21701.bondarenko.mysocnet.dao.CommentsMediaDao;
import jv21701.bondarenko.mysocnet.entityes.CommentsMedia;
import org.springframework.stereotype.Component;

@Component
public class CommentsMediaDaoImpl extends AbstractDao<CommentsMedia, Integer> implements CommentsMediaDao{
    
}
