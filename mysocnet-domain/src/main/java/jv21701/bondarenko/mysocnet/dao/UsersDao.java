package jv21701.bondarenko.mysocnet.dao;

import java.util.List;
import jv21701.bondarenko.mysocnet.entityes.Users;

public interface UsersDao extends BaseDao<Users, Integer> {

    int getUsersCount();

    List<Users> getUsers(int offset, int limit);

    public Users getByUsername(String login);

    Users byUsername(String username);
    
    Users getByVerificationToken(String regcode);
}
