
package jv21701.bondarenko.mysocnet.dao;

import jv21701.bondarenko.mysocnet.entityes.Media;

public interface MediaDao extends BaseDao<Media, Integer>{
    
}
