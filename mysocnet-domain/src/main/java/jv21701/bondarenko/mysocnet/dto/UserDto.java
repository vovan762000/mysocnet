package jv21701.bondarenko.mysocnet.dto;

import java.util.UUID;

public class UserDto {
    
    private final String REGCODE = UUID.randomUUID().toString();

    private String firstname;

    private String lastname;

    private String password;

    private String login;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getREGCODE() {
        return REGCODE;
    }

}
