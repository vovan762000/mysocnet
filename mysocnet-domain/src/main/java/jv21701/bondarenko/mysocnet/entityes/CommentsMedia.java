
package jv21701.bondarenko.mysocnet.entityes;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comments_media")
@NamedQueries({
    @NamedQuery(name = "CommentsMedia.findAll", query = "SELECT c FROM CommentsMedia c")})
public class CommentsMedia implements Serializable {

    private static final long serialVersionUID = 1L;
      @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "comments_media")
    private String commentsMedia;
    
    @Basic(optional = false)
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    @Basic(optional = false)
    @Column(name = "user_com_name")
    private String userComName;
    
    @JoinColumn(name = "media_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Media media;

    public CommentsMedia() {
    }

    public CommentsMedia(Integer id) {
        this.id = id;
    }

    public CommentsMedia(Integer id, String commentsMedia) {
        this.id = id;
        this.commentsMedia = commentsMedia;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCommentsMedia() {
        return commentsMedia;
    }

    public void setCommentsMedia(String commentsMedia) {
        this.commentsMedia = commentsMedia;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUserComName() {
        return userComName;
    }

    public void setUserComName(String userComId) {
        this.userComName = userComId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommentsMedia)) {
            return false;
        }
        CommentsMedia other = (CommentsMedia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jv21701.bondarenko.mysocnet.entityes.CommentsMedia[ id=" + id + " ]";
    }
    
}
