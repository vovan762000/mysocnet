
package jv21701.bondarenko.mysocnet.entityes;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "user_like_post")
@NamedQueries({
    @NamedQuery(name = "UserLikePost.findAll", query = "SELECT u FROM UserLikePost u")})
public class UserLikePost implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "userid")
    private int userid;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "postid")
    private int postid;

    public UserLikePost() {
    }

    public UserLikePost(Integer id) {
        this.id = id;
    }

    public UserLikePost(Integer id, int userid, int postid) {
        this.id = id;
        this.userid = userid;
        this.postid = postid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserLikePost)) {
            return false;
        }
        UserLikePost other = (UserLikePost) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jv21701.bondarenko.mysocnet.entityes.UserLikePost[ id=" + id + " ]";
    }
    
}
