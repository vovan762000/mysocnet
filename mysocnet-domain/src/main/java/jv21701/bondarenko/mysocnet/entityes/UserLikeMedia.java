
package jv21701.bondarenko.mysocnet.entityes;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_like_media")
@NamedQueries({
    @NamedQuery(name = "UserLikeMedia.findAll", query = "SELECT u FROM UserLikeMedia u")})
public class UserLikeMedia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "media_id")
    private int mediaId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private int userId;

    public UserLikeMedia() {
    }

    public UserLikeMedia(Integer id) {
        this.id = id;
    }

    public UserLikeMedia(Integer id, int mediaId, int userId) {
        this.id = id;
        this.mediaId = mediaId;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getMediaId() {
        return mediaId;
    }

    public void setMediaId(int mediaId) {
        this.mediaId = mediaId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserLikeMedia)) {
            return false;
        }
        UserLikeMedia other = (UserLikeMedia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jv21701.bondarenko.mysocnet.entityes.UserLikeMedia[ id=" + id + " ]";
    }
    
}
