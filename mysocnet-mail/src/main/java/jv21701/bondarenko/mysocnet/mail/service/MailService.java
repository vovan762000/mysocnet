
package jv21701.bondarenko.mysocnet.mail.service;

import jv21701.bondarenko.mysocnet.mail.bean.Mail;

public interface MailService {
 
    public void sendEmail(Mail mail);
}
