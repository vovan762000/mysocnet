package jv21701.bondarenko.mysocnet.serviceImpl;

import java.io.Serializable;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.MessagesDao;
import jv21701.bondarenko.mysocnet.entityes.Messages;
import jv21701.bondarenko.mysocnet.service.MessagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class MessagesServiceImpl implements MessagesService {

    @Autowired
    private MessagesDao messagesDao;

    @Override
    public Messages byId(Integer id) {
        return messagesDao.byId(id);
    }

    @Override
    public Serializable save(Messages object) {
        return messagesDao.save(object);
    }

    @Override
    public void update(Messages object) {
        messagesDao.update(object);
    }

    @Override
    public void delete(Messages object) {
        messagesDao.remove(object);
    }

    @Override
    public List<Messages> list() {
        return messagesDao.list();
    }

}
