package jv21701.bondarenko.mysocnet.serviceImpl;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.UsersDao;
import jv21701.bondarenko.mysocnet.dto.UserDto;
import jv21701.bondarenko.mysocnet.entityes.Users;
import jv21701.bondarenko.mysocnet.entityes.Users.Role;
import jv21701.bondarenko.mysocnet.service.AdminUsersListFrameObject;
import jv21701.bondarenko.mysocnet.service.AdminUsersObject;
import jv21701.bondarenko.mysocnet.service.UserService;
import jv21701.bondarenko.mysocnet.service.UsersObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersDao usersDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Users byId(Integer id) {
        return usersDao.byId(id);
    }

    @Override
    public Serializable save(Users object) {
        return usersDao.save(object);
    }

    @Override
    public void update(Users object) {
        usersDao.update(object);
    }

    @Override
    public void delete(Users object) {
        usersDao.remove(object);
    }

    @Override
    public List<Users> list() {
        return usersDao.list();
    }

    @Override
    public UsersObject getUserById(Integer id) {
        //Users user = usersDao.byId(id);
        return new UsersObject(usersDao.byId(id));
    }

    @Override
    public AdminUsersObject getFullUserById(Integer id) {
        return new AdminUsersObject(usersDao.byId(id));
    }

    @Override
    public AdminUsersListFrameObject getUsersFrame(int offset, int limit) {
        int totalUsers = usersDao.getUsersCount();
        List<Users> users = usersDao.getUsers(offset, limit);
        return new AdminUsersListFrameObject(users, totalUsers);
    }

    @Override
    public Users byUsername(String username) {
        return usersDao.byUsername(username);
    }

    @Override
    public void registerNewUserAccount(UserDto accountDto) {
        Users user = new Users();
        user.setFirstname(accountDto.getFirstname());
        user.setLastname(accountDto.getLastname());
        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        user.setLogin(accountDto.getLogin());
        user.setRole(Role.ROLE_USER);
        user.setRegstatus(Boolean.FALSE);
        user.setRegcode(accountDto.getREGCODE());
        user.setAvatar("files" + File.separator + "noavatar.png");
//        user.setMediaList(new ArrayList<>());
//        user.setPostsList(new ArrayList<Posts>());
        save(user);
    }

    @Override
    public Users getByVerificationToken(String regcode) {
        return usersDao.getByVerificationToken(regcode);
    }
}
