package jv21701.bondarenko.mysocnet.serviceImpl;

import java.io.Serializable;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.CommentsMediaDao;
import jv21701.bondarenko.mysocnet.entityes.CommentsMedia;
import jv21701.bondarenko.mysocnet.service.CommentsMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class CommentsMediaServiceImpl implements CommentsMediaService {

    @Autowired
    private CommentsMediaDao commentsMediaDao;

    @Override
    public CommentsMedia byId(Integer id) {
        return commentsMediaDao.byId(id);
    }

    @Override
    public Serializable save(CommentsMedia object) {
        return commentsMediaDao.save(object);
    }

    @Override
    public void update(CommentsMedia object) {
        commentsMediaDao.update(object);
    }

    @Override
    public void delete(CommentsMedia object) {
        commentsMediaDao.remove(object);
    }

    @Override
    public List<CommentsMedia> list() {
        return commentsMediaDao.list();
    }

}
