
package jv21701.bondarenko.mysocnet.serviceImpl;

import java.io.Serializable;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.PostsDao;
import jv21701.bondarenko.mysocnet.entityes.Posts;
import jv21701.bondarenko.mysocnet.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class PostsServiceImpl implements PostsService{
    
    @Autowired
    private PostsDao postsDao;

    @Override
    public Posts byId(Integer id) {
        return postsDao.byId(id);
    }

    @Override
    public Serializable save(Posts object) {
        return postsDao.save(object);
    }

    @Override
    public void update(Posts object) {
         postsDao.update(object);
    }

    @Override
    public void delete(Posts object) {
        postsDao.remove(object);
    }

    @Override
    public List<Posts> list() {
        return postsDao.list();
    }
    
}
