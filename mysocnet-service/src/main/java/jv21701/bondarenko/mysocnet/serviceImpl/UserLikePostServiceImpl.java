package jv21701.bondarenko.mysocnet.serviceImpl;

import java.io.Serializable;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.UserLikePostDao;
import jv21701.bondarenko.mysocnet.entityes.UserLikePost;
import jv21701.bondarenko.mysocnet.service.UserLikePostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class UserLikePostServiceImpl implements UserLikePostService {

    @Autowired
    private UserLikePostDao userLikePostDao;

    @Override
    public UserLikePost byId(Integer id) {
        return userLikePostDao.byId(id);
    }

    @Override
    public Serializable save(UserLikePost object) {
        return userLikePostDao.save(object);
    }

    @Override
    public void update(UserLikePost object) {
        userLikePostDao.update(object);
    }

    @Override
    public void delete(UserLikePost object) {
        userLikePostDao.remove(object);
    }

    @Override
    public List<UserLikePost> list() {
        return userLikePostDao.list();
    }

}
