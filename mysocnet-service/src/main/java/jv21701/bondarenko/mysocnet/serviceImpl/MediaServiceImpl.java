package jv21701.bondarenko.mysocnet.serviceImpl;

import java.io.Serializable;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.MediaDao;
import jv21701.bondarenko.mysocnet.entityes.Media;
import jv21701.bondarenko.mysocnet.service.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class MediaServiceImpl implements MediaService {

    @Autowired
    MediaDao mediaDao;

    @Override
    public Media byId(Integer id) {
        return mediaDao.byId(id);
    }

    @Override
    public Serializable save(Media object) {
        return mediaDao.save(object);
    }

    @Override
    public void update(Media object) {
        mediaDao.update(object);
    }

    @Override
    public void delete(Media object) {
        mediaDao.remove(object);
    }

    @Override
    public List<Media> list() {
        return mediaDao.list();
    }

}
