
package jv21701.bondarenko.mysocnet.serviceImpl;

import java.io.Serializable;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.FriendsDao;
import jv21701.bondarenko.mysocnet.entityes.Friends;
import jv21701.bondarenko.mysocnet.service.FriendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class FriendsServiceImpl implements FriendsService{
    
    @Autowired
    private FriendsDao friendsDao;

    @Override
    public Friends byId(Integer id) {
        return friendsDao.byId(id);
    }

    @Override
    public Serializable save(Friends object) {
        return friendsDao.save(object);
    }

    @Override
    public void update(Friends object) {
        friendsDao.update(object);
    }

    @Override
    public void delete(Friends object) {
        friendsDao.remove(object);
    }

    @Override
    public List<Friends> list() {
        return friendsDao.list();
    }
    
}
