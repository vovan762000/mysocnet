package jv21701.bondarenko.mysocnet.serviceImpl;

import java.io.Serializable;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.CommentsDao;
import jv21701.bondarenko.mysocnet.entityes.Comments;
import jv21701.bondarenko.mysocnet.service.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class CommentsServiceImpl implements CommentsService {

    @Autowired
    private CommentsDao commentsDao;

    @Override
    public Comments byId(Integer id) {
        return commentsDao.byId(id);
    }

    @Override
    public Serializable save(Comments object) {
        return commentsDao.save(object);
    }

    @Override
    public void update(Comments object) {
        commentsDao.update(object);
    }

    @Override
    public void delete(Comments object) {
        commentsDao.remove(object);
    }

    @Override
    public List<Comments> list() {
        return commentsDao.list();
    }

}
