
package jv21701.bondarenko.mysocnet.serviceImpl;

import java.io.Serializable;
import java.util.List;
import jv21701.bondarenko.mysocnet.dao.UserLikeMediaDao;
import jv21701.bondarenko.mysocnet.entityes.UserLikeMedia;
import jv21701.bondarenko.mysocnet.service.UserLikeMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class UserLikeMediaServiceImpl implements UserLikeMediaService{

    @Autowired
    private UserLikeMediaDao userLikeMediaDao;
    
    @Override
    public UserLikeMedia byId(Integer id) {
        return userLikeMediaDao.byId(id);
    }

    @Override
    public Serializable save(UserLikeMedia object) {
        return userLikeMediaDao.save(object);
    }

    @Override
    public void update(UserLikeMedia object) {
        userLikeMediaDao.update(object);
    }

    @Override
    public void delete(UserLikeMedia object) {
        userLikeMediaDao.remove(object);
    }

    @Override
    public List<UserLikeMedia> list() {
        return userLikeMediaDao.list();
    }
    
}
