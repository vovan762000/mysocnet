
package jv21701.bondarenko.mysocnet.service;

import jv21701.bondarenko.mysocnet.entityes.Users;

public class UserAuth extends Users{

    public UserAuth(Users user) {
        super.setId(user.getId());
        super.setFirstname(user.getFirstname());
        super.setLastname(user.getLastname());
        super.setLogin(user.getLogin());
        super.setPassword(user.getPassword());
        super.setRole(user.getRole());
        super.setRegstatus(user.getRegstatus());
        super.setAvatar(user.getAvatar());
    }
}
