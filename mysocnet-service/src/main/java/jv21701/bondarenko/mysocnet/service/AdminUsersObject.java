
package jv21701.bondarenko.mysocnet.service;

import jv21701.bondarenko.mysocnet.entityes.Users;

public class AdminUsersObject extends UsersObject {

    private String login;
    private String firstname;
    private String lastname;
    private boolean regstatus;

    public AdminUsersObject() {
    }

    public AdminUsersObject(Users user) {
        super(user);
        this.login = user.getLogin();
        this.regstatus = user.getRegstatus();
        this.firstname = user.getFirstname();
        this.lastname = user.getLastname();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public boolean isRegstatus() {
        return regstatus;
    }

    public void setRegstatus(boolean regstatus) {
        this.regstatus = regstatus;
    }

}
