
package jv21701.bondarenko.mysocnet.service;

import java.util.Date;
import jv21701.bondarenko.mysocnet.entityes.Media;

public class MediaObject {
    private Integer id;
    private String media;
    private Date dateCreated;
    private Media.MediaType mediaType;

    public MediaObject() {
    }

     public MediaObject(Media m) {
         this.id = m.getId();
         this.media = m.getMedia();
         this.dateCreated = m.getDateCreated();
         this.mediaType = m.getMediaType();
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Media.MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(Media.MediaType mediaType) {
        this.mediaType = mediaType;
    }
    
    
}
