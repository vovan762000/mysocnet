package jv21701.bondarenko.mysocnet.service;

import jv21701.bondarenko.mysocnet.dto.UserDto;
import jv21701.bondarenko.mysocnet.entityes.Users;

public interface UserService extends BaseService<Users, Integer> {

    UsersObject getUserById(Integer id);

    AdminUsersObject getFullUserById(Integer id);

    AdminUsersListFrameObject getUsersFrame(int offset, int limit);

    Users byId(Integer id);

    Users byUsername(String username);

    void registerNewUserAccount(UserDto accountDto);
    
    Users getByVerificationToken(String regcode);
}
