
package jv21701.bondarenko.mysocnet.service;

import java.util.ArrayList;
import java.util.List;
import jv21701.bondarenko.mysocnet.entityes.Media;
import jv21701.bondarenko.mysocnet.entityes.Posts;
import jv21701.bondarenko.mysocnet.entityes.Users;

public class UsersObject {
    private Integer id;
    private String firstname;
    private String lastname;
    private String avatar;
    private List<PostObject> posts;
    private List<MediaObject> media;

    public UsersObject() {
    }

      public UsersObject(Users user) {
          this.id = user.getId();
          this.firstname = user.getFirstname();
          this.lastname = user.getLastname();
          this.avatar = user.getAvatar();
          
          this.posts = new ArrayList<>();
          for (Posts p : user.getPostsList()) {
              this.posts.add(new PostObject(p));
          }
          
          this.media = new ArrayList<>();
          for (Media p : user.getMediaList()) {
              this.media.add(new MediaObject(p));
          }
          
    } 
      
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<PostObject> getPosts() {
        return posts;
    }

    public void setPosts(List<PostObject> posts) {
        this.posts = posts;
    }

    public List<MediaObject> getMedia() {
        return media;
    }

    public void setMedia(List<MediaObject> media) {
        this.media = media;
    }
    
    
    
}

